# Iteracion 1 · 1-2021

## 🔖 Sobre el proyecto

La pandemia -junto a la inoperancia del gobierno- ha causado una profundización de la desigualdad de ingresos, ausencia de sueldos aun estando contratades y las personas necesitan una nueva fuente de ingresos. Es por esto que se crea una aplicación que, con coordinación de municipios, les ayude a vecines a apoyar el mercado local con anuncios comunitarios.

### Características actuales

- Página de tipo restrictiva, se requiere iniciar sesión para ver el contenido sensible de vecines.
- Publicación y edición de anuncios por parte de usuarios.
- Categorias con productos

## ⚙️ Herramientas

Vue.js + Nginx

## 🌱 Estructura GIT - A falta de Gitflow

Con la finalidad de ordenarnos, procedo a estandarizar como usaremos las ramas.

### Master = Producción

Es la rama principal de proyecto , donde va el código para generar la aplicación en entornos de producción. Sobre ella no se debería desarrollar.

Si detectamos un error en el ambiente de producción, hacemos una ramita llamada:

### Hotfix = Solución de Incidencias

Este tipo de ramas se crean para la solución de un incidente. Debe ser generada a partir de Master. También se relaciona con el término “cambios en caliente”. Una vez listo, se fusiona con la rama producción.
**Nombre ramas**

```
hotfix/issue + número incidencia de gitlab
hotfix/issue7
hotfix/issue3
```

### Develop = Desarrollo

En esta rama se tienen las nuevas funcionalidades de la aplicación. No es aconsejable hacer commit (guardar cambios) directamente sobre ella, porque la finalidad de esta rama es ser la copia exacta de master sobre la cual aplicar los cambios sin el riesgo de echarnos el servicio en master.

### Feature = ¡Nueva característica!

Ramas generadas a partir de la rama develop. Sobre ella se harán los desarrollos de nuevas funcionalidades. Una vez esté desarrollada la funcionalidad la rama feature podrá ser mezclada con Develop para que los cambios queden allí sincronizados, y finalmente se procede a borrar la rama feature. Es aconsejable tener esta rama sincronizada con los cambios que se agreguen a la rama develop para evitar conflictos.

**Nombre ramas**

```
feature/caracteristica
feature/login
feature/categoria
```

### Info extra: Realese

Se solía tener la rama release para hacer las pruebas QA pero la finalidad de ésta clase es dejar de hacer testeo manual para pasar a la automatización de éstas tareas. Por lo mismo, no se usará :P Pero ojalá ésta pequeña descripción ayude a entender para que sirven los devops. Nos ahorran mucho tiempo.

## 🔩 Instalación y Uso

### Para levantar servidor local

```
npm install

npm run build

npm run serve
```

### Para corregir

```
npm audit fix

npm run lint
```

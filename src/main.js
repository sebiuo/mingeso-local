import Vue from 'vue'
import router from './router'
import store from './store'
import App from './App.vue'
import vuetify from './plugins/vuetify'
import i18n from './lang'
import Vuelidate from 'vuelidate'
import moment from 'moment'
import firebase from 'firebase'
import accounting from 'accounting'
 
var firebaseConfig = {
  apiKey: "AIzaSyCyqMso90d2AG40sRu_LaTm_9FJXBpErOk",
  authDomain: "mingeso-8f4cc.firebaseapp.com",
  projectId: "mingeso-8f4cc",
  storageBucket: "mingeso-8f4cc.appspot.com",
  messagingSenderId: "738068280880",
  appId: "1:738068280880:web:8b0f64950a9f4014465ff5"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);

Vue.use(Vuelidate)
Vue.config.productionTip = false

Vue.filter('formatMoney', (date) => {
  return accounting.formatMoney(date, '$', 0, '.', ',')
})

Vue.filter('formatDate', (date) => {
  moment.locale('es')
  return moment(date).format('LLL')
})


Vue.filter('formatPhoneNumber', (phoneNumberString) => {
  let cleaned = ('' + phoneNumberString).replace(/\D/g, '')
  let match = cleaned.match(/(\d{1})(\d{4})(\d{4})$/)
  if (match) {
    return [match[1], ' ', match[2], '-', match[3]].join('')
  }
  return null
})

new Vue({
  router,
  store,
  vuetify,
  i18n,
  Vuelidate,
  render: (h) => h(App),
}).$mount('#app')

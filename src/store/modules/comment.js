import axios from 'axios'
import { Connection } from '../../_helpers/connection'

const headers = {
  'x-api-key': '436236939443955C11494D448451F',
  'Content-Type': 'application/json'
}

const comment = {
  namespaced: true,
  state: {
    result: [],
    commentFetching: 'NO_FETCH',
  },
  mutations: {
    // COMMENT
    POST_COMMENT_START(state) {
      state.commentFetching = 'FETCHING'
    },
    POST_COMMENT_SUCCESS(state, data) {
      state.result = data,
      state.commentFetching = 'FETCHED'
    },
    POST_COMMENT_ERROR(state) {
      state.commentFetching = 'ERROR'
    },
    RESET_VALUES_SUCCESS(state) {
      state.result = [],
      state.commentFetching = 'NO_FETCH'
    }
  },
  actions: {
    async postComment({ commit }, data) {
      commit('POST_COMMENT_START')
      await axios
              .post(`http://${Connection.localhost}/api/v1/core/comentarAnunziao`, data, { headers })
              .then((result) => {
                commit('POST_COMMENT_SUCCESS', result.data)
              })
              .catch((e) => {
                console.error(e)
                commit('POST_COMMENT_ERROR')
              })
    },
    resetValues({ commit }) {
      commit('RESET_VALUES_SUCCESS')
    }
  },
}

export default comment
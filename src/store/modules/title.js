import axios from 'axios'
import { Connection } from '../../_helpers/connection'

const title = {
  namespaced: true,
  state: {
    title: '',
    fetching: 'NO_FETCH'
  },
  mutations: {
    // TITLE
    GET_TITLE_START(state) {
      state.fetching = 'FETCHING'
    },
    GET_TITLE_SUCCESS(state, data) {
      state.title = data,
      state.fetching = 'FETCHED'
    },
    GET_TITLE_ERROR(state) {
      state.fetching = 'ERROR'
    }
  },
  actions: {
    async getTitle({ commit }) {
      commit('GET_TITLE_START')
      await axios
              .get(`http://${Connection.localhost}/`)
              .then((result) => {
                commit('GET_TITLE_SUCCESS', result.data)
              })
              .catch((e) => {
                console.error(e)
                commit('GET_TITLE_ERROR')
              })
    }
  }
}

export default title
import axios from 'axios'
import Vue from 'vue'
import VueToast from 'vue-toast-notification'
import 'vue-toast-notification/dist/theme-sugar.css'
import { Role } from '../../_helpers/role'
import router from '../../router'
import { Connection } from '../../_helpers/connection'

Vue.use(VueToast);

const headers = {
  'x-api-key': '436236939443955C11494D448451F',
  'Content-Type': 'application/json'
}

const user = {
  namespaced: true,
  state : {
    user: [],
    signUpFetching: 'NO_FETCH',
    logInFetching: 'NO_FETCH',
  },
  mutations: {
    // SIGNUP
    SIGNUP_USER_START(state) {
      state.signUpFetching = 'FETCHING'
    },
    SIGNUP_USER_SUCCESS(state, data) {
      state.user = data,
      state.signUpFetching = 'FETCHED'
    },
    SIGNUP_USER_ERROR(state) {
      state.signUpFetching = 'ERROR'
    },
    // LOGIN
    LOGIN_USER_START(state) {
      state.logInFetching = 'FETCHING'
    },
    LOGIN_USER_SUCCESS(state, data) {
      state.user = data,
      state.logInFetching = 'FETCHED'
    },
    LOGIN_USER_ERROR(state) {
      state.logInFetching = 'ERROR'
    },
    // LOGOUT
    LOGOUT_USER_SUCCESS(state) {
      state.user = [],
      state.signUpFetching = 'NO_FETCH',
      state.logInFetching = 'NO_FETCH'
    },
    // VALIDATED
    RESET_VALUES_SUCCESS(state) {
      state.logInFetching = 'VALIDATED'
    }
  },
  actions: {
    async signUpUser({ commit }, data) {
      commit('SIGNUP_USER_START')
      await axios
              .post(`http://${Connection.localhost}/api/v1/core/inscripcionCiudadano`, data, { headers })
              .then((result) => {
                commit('SIGNUP_USER_SUCCESS', result.data[0])
                Vue.$toast.info('Usuario registrado exitosamente', {
                  position: 'top-right'
                })
              })
              .catch((e) => {
                console.error(e)
                commit('SIGNUP_USER_ERROR')
                Vue.$toast.error('No se pudo registrar', {
                  position: 'top-right'
                })
              })
    },
    async logInUser({ commit }, data, stayConnected ) {
      commit('LOGIN_USER_START')
      await axios
              .post(`http://${Connection.localhost}/api/v1/core/inicioSesion`, data, { headers })
              .then((result) => {
                if(result.data.status === "error") {
                  commit('LOGIN_USER_ERROR')
                  Vue.$toast.error('No se pudo iniciar sesión', {
                    position: 'top-right'
                  })
                } else {
                  if(stayConnected) {
                    localStorage.setItem('session', JSON.stringify(result.data))
                    localStorage.setItem('login', JSON.stringify(data))
                    //sessionStorage.setItem('authorization', result.data[0].authorization)
                  } else {
                    sessionStorage.setItem('session', JSON.stringify(result.data))
                    sessionStorage.setItem('login', JSON.stringify(data))
                    //sessionStorage.setItem('authorization', result.data[0].authorization)
                  }
  
                  commit('LOGIN_USER_SUCCESS', result.data)
                  Vue.$toast.info(`Bienvenido ${data.email}`, {
                    position: 'top-right'
                  })
                  if(result.data.rol === Role.JuntaVecinal) {
                    router.push('/sign-up')
                  } else if (result.data.rol === Role.Municipalidad) {
                    router.push('/metrics')
                  } else {
                    router.push('/')
                  }
                }

              })
              .catch((e) => {
                console.error(e)
                commit('LOGIN_USER_ERROR')
                Vue.$toast.error('No se pudo iniciar sesión', {
                  position: 'top-right'
                })
              })
    },
    async logOutUser({ commit }) {
      commit('LOGOUT_USER_SUCCESS')
      localStorage.clear()
      sessionStorage.clear()
      Vue.$toast.info('Has cerrado sesión', {
        position: 'top-right'
      })
      router.push('/login')
    },
    resetValues({ commit }) {
      commit('RESET_VALUES_SUCCESS')
    }
  }
}

export default user
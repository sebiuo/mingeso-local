import axios from 'axios'
import Vue from 'vue'
import VueToast from 'vue-toast-notification'
import 'vue-toast-notification/dist/theme-sugar.css'
import { Connection } from '../../_helpers/connection'

Vue.use(VueToast);

const headers = {
  'x-api-key': '436236939443955C11494D448451F',
  'Content-Type': 'application/json'
}

const advertisements = {
  namespaced: true,
  state: {
    advertisement: [],
    advertisements: [],
    userAdvertisements: [],
    advertisementFetching: 'NO_FETCH',
    advertisementsFetching: 'NO_FETCH',
    userAdvertisementsFetching: 'NO_FETCH',
    userChangeLikeFetching: 'NO_FETCH',
  },
  mutations: {
    // ADVERTISEMENT
    GET_ADVERTISEMENT_START(state) {
      state.advertisementFetching = 'FETCHING'
    },
    GET_ADVERTISEMENT_SUCCESS(state, data) {
      state.advertisement = data
      state.advertisementFetching = 'FETCHED'
    },
    GET_ADVERTISEMENT_ERROR(state) {
      state.advertisementFetching = 'ERROR'
    },

    // ALL ADVERTISEMENTS
    GET_ADVERTISEMENTS_START(state) {
      state.advertisementsFetching = 'FETCHING'
    },
    GET_ADVERTISEMENTS_SUCCESS(state, data) {
      state.advertisements = data
      state.advertisementsFetching = 'FETCHED'
    },
    GET_ADVERTISEMENTS_ERROR(state) {
      state.advertisementsFetching = 'ERROR'
    },
    
    // USER ADVERTISEMENTS
    GET_USER_ADVERTISEMENTS_START(state) {
      state.userAdvertisementsFetching = 'FETCHING'
    },
    GET_USER_ADVERTISEMENTS_SUCCESS(state, data) {
      state.userAdvertisements = data
      state.userAdvertisementsFetching = 'FETCHED'
    },
    GET_USER_ADVERTISEMENTS_ERROR(state) {
      state.userAdvertisementsFetching = 'ERROR'
    },

    // POST DATOS FILTRADOS
    POST_DATA_FILTER_SUCCESS(state, data) {
      state.advertisements = data
    },

    // POST CAMBIO DE LIKE
    POST_CHANGE_LIKE_ADVERTISEMENTS_START(state){
      state.userChangeLikeFetching= 'FETCHING'
    },
    POST_CHANGE_LIKE_ADVERTISEMENTS_SUCCESS(state){
      state.advertisement.userLikeAnuncio=!state.advertisement.userLikeAnuncio
      if (state.advertisement.userLikeAnuncio) {
        state.advertisement.likes++
      }else{
        state.advertisement.likes--
      }
      state.userChangeLikeFetching= 'FETCHED'
    },
    POST_CHANGE_LIKE_ADVERTISEMENTS_ERROR(state){
      state.userChangeLikeFetching= 'ERROR'
    },

  },
  actions: {
    async getAdvertisement({ commit }, data) {
      commit('GET_ADVERTISEMENT_START')
      await axios
              .post(`http://${Connection.localhost}/api/v1/core/seleccionarAnunziao`, data, { headers })
              .then((result) => {
                commit('GET_ADVERTISEMENT_SUCCESS', result.data.anunziao)
              })
              .catch((e) => {
                console.error(e)
                commit('GET_ADVERTISEMENT_ERROR')
              })
    },
    async getAdvertisements({ commit }, data) {
      commit('GET_ADVERTISEMENTS_START')
      await axios
              .post(`http://${Connection.localhost}/api/v1/core/obtenerAnunziaos`, data, { headers })
              .then((result) => {
                result.data.anunziaos.map(x => {
                  x.fecha_creacion = new Date(x.fecha_creacion).toLocaleString("es-CL", { timeZone: 'America/Santiago' }).split(' ')[0];
                  if (x.promocion ) x.promocion.fecha_termino = new Date(x.promocion.fecha_termino).toLocaleString("es-CL", { timeZone: 'America/Santiago' }).split(' ')[0];
                })
                commit('GET_ADVERTISEMENTS_SUCCESS', result.data.anunziaos)
              })
              .catch((e) => {
                console.error(e)
                commit('GET_ADVERTISEMENTS_ERROR')
              })
    },
    async getUserAdvertisements({ commit }, data) {
      let allAdvertisements
      let userAdvertisements
      commit('GET_USER_ADVERTISEMENTS_START')
      await axios
              .post(`http://${Connection.localhost}/api/v1/core/obtenerAnunziaos`, data, { headers })
              .then(result => {
                allAdvertisements = result.data.anunziaos          
              })
              .catch((e) => {
                console.error(e)
                commit('GET_USER_ADVERTISEMENTS_ERROR')
                return
              })

        allAdvertisements.map(x => {
          x.fecha_creacion = new Date(x.fecha_creacion).toLocaleString("es-CL", { timeZone: 'America/Santiago' }).split(' ')[0];
          if (x.promocion ) x.promocion.fecha_termino = new Date(x.promocion.fecha_termino).toLocaleString("es-CL", { timeZone: 'America/Santiago' }).split(' ')[0];
      })
      userAdvertisements = allAdvertisements.filter(x => x.id_user_publicador === data.id_usuario)
      commit('GET_USER_ADVERTISEMENTS_SUCCESS', userAdvertisements)
    },
    async postDatosFiltrados({ commit }, data) {
      commit('POST_DATA_FILTER_SUCCESS', data)
      Vue.$toast.info('Datos filtrados obtenidos exitosamente!', {
        position: 'top-right'
      })
    },
    async switchOfLike({ commit },data) {
      commit('POST_CHANGE_LIKE_ADVERTISEMENTS_START')
      await axios
              .post(`http://${Connection.localhost}/api/v1/core/switchOfLike`, data, { headers })
              .then(result => {
                console.log(result);
                commit('POST_CHANGE_LIKE_ADVERTISEMENTS_SUCCESS')
                return result.data.status
              })
              .catch((e) => {
                console.error(e)
                commit('GET_USER_ADVERTISEMENTS_ERROR')
                return
              })
    },
  },
}

export default advertisements

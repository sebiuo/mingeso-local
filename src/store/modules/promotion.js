import axios from 'axios'
import Vue from 'vue'
import VueToast from 'vue-toast-notification'
import 'vue-toast-notification/dist/theme-sugar.css'
import { Connection } from '../../_helpers/connection'

Vue.use(VueToast);

const headers = {
  'x-api-key': '436236939443955C11494D448451F',
  'Content-Type': 'application/json'
}

const promotion = {
  namespaced: true,
  state: {
    result: [],
    promotionFetching: 'NO_FETCH',
  },
  mutations: {
    // PROMOTION
    POST_PROMOTION_START(state) {
      state.promotionFetching = 'FETCHING'
    },
    POST_PROMOTION_SUCCESS(state, data) {
      state.result = data,
      state.promotionFetching = 'FETCHED'
    },
    POST_PROMOTION_ERROR(state) {
      state.promotionFetching = 'ERROR'
    },
    RESET_VALUES_SUCCESS(state) {
      state.result = [],
      state.promotionFetching = 'NO_FETCH'
    }
  },
  actions: {
    async postPromotion({ commit }, data) {
      commit('POST_PROMOTION_START')
      await axios
              .post(`http://${Connection.localhost}/api/v1/core/crearPromocion`, data, { headers })
              .then((result) => {
                if(result.data.status === "error") {
                  commit('POST_PROMOTION_ERROR')
                  Vue.$toast.error(`${result.data.message.toUpperCase()}`, {
                    position: 'top-right'
                  })
                } else {
                  commit('POST_PROMOTION_SUCCESS', result.data)
                  Vue.$toast.info(`Promoción creada exitosamente!`, {
                    position: 'top-right'
                  })
                }
              })
              .catch((e) => {
                console.error(e)
                commit('POST_PROMOTION_ERROR')
              })
    },
    resetValues({ commit }) {
      commit('RESET_VALUES_SUCCESS')
    }
  },
}

export default promotion
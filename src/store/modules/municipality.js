import axios from 'axios'
import Vue from 'vue'
import VueToast from 'vue-toast-notification'
import 'vue-toast-notification/dist/theme-sugar.css'
import { Connection } from '../../_helpers/connection'

Vue.use(VueToast);

const headers = {
  'x-api-key': '436236939443955C11494D448451F',
  'Content-Type': 'application/json'
}

const municipality = {
  namespaced: true,
  state: {
    rankingCiudadanos: [],
    rankingJV: [],
    fetching: 'NO_FETCH',
    fetchingJV: 'NO_FETCH'
  },
  mutations: {
    // RANKING
    POST_RANKING_START(state) {
      state.fetching = 'FETCHING'
    },
    POST_RANKING_SUCCESS(state, data) {
      state.rankingCiudadanos = data,
      state.fetching = 'FETCHED'
    },
    POST_RANKING_ERROR(state) {
      state.fetching = 'ERROR'
    },
    // RANKING JV
    POST_RANKING_JV_START(state) {
      state.fetchingJV = 'FETCHING'
    },
    POST_RANKING_JV_SUCCESS(state, data) {
      state.rankingJV = data,
      state.fetchingJV = 'FETCHED'
    },
    POST_RANKING_JV_ERROR(state) {
      state.fetchingJV = 'ERROR'
    }
  },
  actions: {
    async postRanking({ commit }, data) {
      commit('POST_RANKING_START')
      await axios
              .post(`http://${Connection.localhost}/api/v1/core/obtenerRankingCiudadanos`, data, { headers })
              .then((result) => {
                commit('POST_RANKING_SUCCESS', result.data)
              })
              .catch((e) => {
                console.error(e)
                commit('POST_RANKING_ERROR')
              })
    },
    async postRankingJV({ commit }, data) {
    commit('POST_RANKING_JV_START')
    await axios
            .post(`http://${Connection.localhost}/api/v1/core/rankingJunaVecinalPublicacionesPorFecha`, data, { headers })
            .then((result) => {
              commit('POST_RANKING_JV_SUCCESS', result.data)
              Vue.$toast.info('Datos filtrados obtenidos exitosamente!', {
                position: 'top-right'
              })
            })
            .catch((e) => {
              console.error(e)
              commit('POST_RANKING_JV_ERROR')
              Vue.$toast.error('No se ha podido obtener los datos, inténtentelo más tarde!', {
                position: 'top-right'
              })
            })
  }
  },
}

export default municipality
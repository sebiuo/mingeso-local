import Vue from 'vue'
import VueRouter from 'vue-router'

import Login from '../views/Login.vue'
import SignUp from '../views/SignUp.vue'
import Home from '../views/Home.vue'
import PostDetailView from '../views/PostDetailView.vue'
import NewAdvertisementView from '../views/NewAdvertisementView.vue'
import MetricsView from '../views/MetricsView.vue'
import Publications from '../views/Publications.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/login',
    name: 'Login',
    component: Login,
  },
  {
    path: '/sign-up',
    name: 'SignUp',
    component: SignUp,
  },
  {
    path: '/',
    name: 'Home',
    component: Home,
  },
  {
    path: '/post/:id',
    name: 'Post',
    component: PostDetailView,
  },
  {
    path: '/create-anunziao',
    name: 'NewAdvertisementView',
    component: NewAdvertisementView,
  },
  {
    path: '/publications',
    name: 'Publications',
    component: Publications
  },
  {
    path: '/metrics',
    name: 'MetricsView',
    component: MetricsView,
  },
  {
    path: '*',
    redirect: '/',
  },
]

const router = new VueRouter({
  mode: 'history',
  routes,
})

export default router
